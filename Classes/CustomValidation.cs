using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Kanoon.Classes
{
    public class CustomValidation
    {
        public class DateLessThanOrEqualToToday : ValidationAttribute
        {
            public override string FormatErrorMessage(string name)
            {
                return "تاریخ وارد شده معتبر نیست";
            }

            protected override ValidationResult IsValid(object objValue, ValidationContext validationContext)
            {
                string strValue = objValue.ToString();
                var dateValue = DateTime.Parse(strValue);
                PersianCalendar pc = new PersianCalendar();
                var date = pc.ToDateTime(dateValue.Date.Year, dateValue.Date.Month, dateValue.Date.Day, 0,
                    0 , 0, 0);

                if (date > DateTime.Now)
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }

                return ValidationResult.Success;
            }
        }

        public class DateMoreThanToday : ValidationAttribute
        {
            public override string FormatErrorMessage(string name)
            {
                return "تاریخ وارد شده معتبر نیست";
            }

            protected override ValidationResult IsValid(object objValue, ValidationContext validationContext)
            {
                string strValue = objValue.ToString();
                var dateValue = DateTime.Parse(strValue);
                PersianCalendar pc = new PersianCalendar();
                var date = pc.ToDateTime(dateValue.Date.Year, dateValue.Date.Month, dateValue.Date.Day, 0,
                    0, 0, 0);

                if (date < DateTime.Now.Date)
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }

                return ValidationResult.Success;
            }
        }
    }
}
