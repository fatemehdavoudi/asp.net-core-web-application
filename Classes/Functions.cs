using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using Kanoon.Models;

namespace Kanoon
{
    public static class Functions
    {
        public static string GetDateString(this DateTime date)
        {
            var day = date.Day;
            var month = date.Month;
            var year = date.Year;
            return year.ToString() + "/" + month.ToString() + "/" + day.ToString();
        }

        public static string GetTimeString(this DateTime date)
        {
            var hour = date.Hour;
            var min = date.Minute;
            return hour.ToString() + ":" + min.ToString();
        }

        public static string BanExpretion(this User user)
        {
            return "حساب کاربری شما از تاریخ " + user.BanStart.GetDateString() + " ساعت " + user.BanStart.GetTimeString() +
                " تا تاریخ " + user.BanEnd.GetDateString() + " ساعت " + user.BanEnd.GetTimeString() + " مسدود است ";
        }
    }
}