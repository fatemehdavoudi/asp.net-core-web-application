using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kanoon.Models;
using Kanoon.Models.AccountViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Kanoon.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;

        public AccountController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IEmailSender emailSender,
            ILoggerFactory loggerFactory)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = loggerFactory.CreateLogger<AccountController>();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            ViewBag.ErrorMessage = "";
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    if (user.IsAdmin)
                    {
                        await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                        _logger.LogInformation(1, "کاربر وارد شد.");
                        return RedirectToAction("Index", "Admin");
                    }
                    else if (user.IsActive)
                    {
                        await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                        _logger.LogInformation(1, "کاربر وارد شد.");
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "حساب کاربری شما غیرفعال است";
                        return View(model);
                    }
                }
                else if (user == null)
                {
                    ViewBag.ErrorMessage = "ایمیل وارد شده ثبت نشده است";
                    return View(model);
                }
            }
            return View();
        }


        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            var activation = false;
            ViewBag.ErrorMessage = "";
            if (ModelState.IsValid)
            {
                if (model.IsAdmin)
                {
                    activation = true;
                }

                var user = new User
                {
                    Name = model.Name,
                    LastName = model.LastName,
                    NationalCode = model.NationalCode,
                    BirthDate = model.BirthDate,
                    Email = model.Email,
                    UserName = model.Email,
                    Password = model.Password,
                    IsAdmin = model.IsAdmin,
                    EmailConfirmed = true,
                    IsActive = activation
                };
                var username = _userManager.NormalizeName(model.Email);
                var _user = await _userManager.FindByNameAsync(username);
                if (_user == null)
                {
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        if (user.IsAdmin)
                        {
                            await _userManager.AddToRoleAsync(user, "Admin");
                        }
                        else
                        {
                            await _userManager.AddToRoleAsync(user, "User");
                        }
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                        await _emailSender.SendEmailAsync(model.Email, "حساب کاربری خود را تایید کنید.",
                            "لطفا با کلیل کردن بر روی لینک زیر، حساب کاربری خود را تایید کنید: <a href=\"" + callbackUrl + "\">link</a>");
                        _logger.LogInformation(3, "User created a new account with password.");
                        return View("RegisterMessage");
                    }
                    AddErrors(result);
                }
                else if (_user != null)
                {
                    ViewBag.ErrorMessage = "یک حساب کاربری با این ایمیل ثبت شده است";
                    return View(model);
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        #endregion
    }
}