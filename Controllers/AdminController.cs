using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Kanoon.Data;
using Kanoon.Models;
using Kanoon.Models.MultipleModels;
using Kanoon.Models.UserViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Kanoon.Controllers
{
    public class AdminController : Controller
    {
        private string _username;
        private readonly UserManager<User> _userManager;
        private readonly ApplicationDbContext _context;
        public List<User> _allusers { get; set; }
        public List<Report> _usersReports { get; set; }

        public AdminController(
            IHttpContextAccessor httpContextAccessor,
            UserManager<User> userManager,
            ApplicationDbContext context)
        {
            _username = httpContextAccessor.HttpContext.User.Identity.Name;
            _userManager = userManager;
            _context = context;
            _allusers = new List<User>();
            _usersReports = new List<Report>();
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Profile()
        {
            var username = _userManager.NormalizeName(_username);
            var admin = await _userManager.FindByNameAsync(username);
            return View(admin);
        }

        public async Task<IActionResult> Edit()
        {
            var username = _userManager.NormalizeName(_username);
            var admin = await _userManager.FindByNameAsync(username);
            EditViewModel model = new EditViewModel();
            model.Name = admin.Name;
            model.LastName = admin.LastName;
            model.NationalCode = admin.NationalCode;
            model.BirthDate = admin.BirthDate;
            model.Email = admin.Email;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditViewModel model)
        {
            TempData["Message"] = "";
            TempData["ErrorMessage"] = "";
            var username = _userManager.NormalizeName(_username);
            var admin = await _userManager.FindByNameAsync(username);
            if (ModelState.IsValid)
            {
                admin.Name = model.Name;
                admin.LastName = model.LastName;
                admin.NationalCode = model.NationalCode;
                admin.BirthDate = model.BirthDate;
                admin.Email = model.Email;
                var result = await _userManager.UpdateAsync(admin);
                if (result.Succeeded)
                {
                    try
                    {
                        _context.Update(admin);
                        _context.SaveChanges();
                        TempData["Message"] = "ویرایش اطلاعات موفقیت انجام شد";
                        return RedirectToAction("Profile");
                    }catch(Exception)
                    {
                        TempData["ErrorMessage"] = "مجددا تلاش کنید";
                        return View(model);
                    }
                }
            }
            TempData["ErrorMessage"] = "ویرایش اطلاعات با شکست مواجه شد";
            return View(model);
        }

        public async Task<IActionResult> AllUsers()
        {
            PersianCalendar pc = new PersianCalendar();
            _allusers = await _context.Users.ToListAsync();
            foreach (User u in _allusers)
            {
                var Start = pc.ToDateTime(u.BanStart.Year, u.BanStart.Month, u.BanStart.Day, u.BanStart.Hour,
                    u.BanStart.Minute, u.BanStart.Second, u.BanStart.Millisecond);
                var End = pc.ToDateTime(u.BanEnd.Year, u.BanEnd.Month, u.BanEnd.Day, u.BanEnd.Hour,
                    u.BanEnd.Minute, u.BanEnd.Second, u.BanEnd.Millisecond);
                if (DateTime.Compare(DateTime.Now, End) < 0 && DateTime.Compare(DateTime.Now, Start) >= 0)
                {
                    u.IsBan = true;
                    _context.Update(u);
                    _context.SaveChanges();
                }
                else if (DateTime.Compare(DateTime.Now, End) >= 0 || DateTime.Compare(DateTime.Now, Start) < 0)
                {
                    u.IsBan = false;
                    u.BanStart = new DateTime();
                    u.BanEnd = new DateTime();
                    _context.Update(u);
                    _context.SaveChanges();
                }
            }
            _allusers = await _context.Users.ToListAsync();
            return View(_allusers);
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }


        public async Task<IActionResult> Active(string id)
        {
            TempData["Message"] = "";
            TempData["ErrorMessage"] = "";
            ViewBag.ErrorMessage = "";
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            user.IsActive = true;
            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                try
                {
                    _context.Update(user);
                    _context.SaveChanges();
                    TempData["Message"] = "کاربر با موفقیت فعال شد";
                    return RedirectToAction("AllUsers");
                }catch(Exception)
                {
                    TempData["ErrorMessage"] = "مجددا تلاش کنید";
                    return View("AllUsers");
                }
            }
            TempData["ErrorMessage"] = "عملیات فعال سازی کاربر با شکست مواجه شد";
            return View("AllUsers");
        }


        public async Task<IActionResult> Deactive(string id)
        {
            TempData["Message"] = "";
            TempData["ErrorMessage"] = "";
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            user.IsActive = false;
            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                try
                {
                    _context.Update(user);
                    _context.SaveChanges();
                    TempData["Message"] = "کاربر با موفقیت غیرفعال شد";
                    return RedirectToAction("AllUsers");
                }catch(Exception)
                {
                    TempData["ErrorMessage"] = "مجددا تلاش کنید";
                    return View("AllUsers");
                }
            }
            TempData["ErrorMessage"] = "عملیات غیرفعال سازی کاربر با شکست مواجه شد";
            return View("AllUsers");
        }

        [HttpGet]
        public IActionResult Ban()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Ban(BanViewModel model, string id)
        {
            TempData["Message"] = "";
            TempData["ErrorMessage"] = "";
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ModelState.IsValid)
            {
                var Start = model.StartDate.Add(model.StartTime);
                var End = model.EndDate.Add(model.EndTime);
                user.BanEnd = End;
                user.BanStart = Start;
                user.IsBan = true;
                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    try
                    {
                        _context.Update(user);
                        _context.SaveChanges();
                        TempData["Message"] = "کاربر با موفقیت مسدود شد";
                        return View(model);
                    }
                    catch (Exception)
                    {
                        TempData["ErrorMessage"] = "مجددا تلاش کنید";
                        return RedirectToAction("AllUsers");
                    }
                }
            }
            TempData["ErrorMessage"] = "عملیات مسدود سازی کاربر با شکست مواجه شد";
            return View(model);
        }
        
        public async Task<IActionResult> Delete(string id)
        {
            TempData["Message"] = "";
            TempData["ErrorMessage"] = "";
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                TempData["ErrorMessage"] = "کاربر مورد نظر یافت نشد";
                return RedirectToAction("AllUsers");
            }
            try
            {
                _context.Users.Remove(user);
                await _context.SaveChangesAsync();
                TempData["Message"] = "کاربر با موفقیت حذف شد";
                return RedirectToAction("AllUsers");
            }catch(Exception)
            {
                TempData["ErrorMessage"] = "مجددا تلاش کنید";
                return RedirectToAction("AllUsers");
            }
        }

        [HttpGet]
        public async Task<IActionResult> VisitedReports()
        {
            ReportUserViewModel model = new ReportUserViewModel();
            model.GetUsers = await _context.Users.ToListAsync();
            model.GetReports = await _context.Reports.ToListAsync();
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> UnvisitedReports()
        {
            ReportUserViewModel model = new ReportUserViewModel();
            model.GetReports = new List<Report>();
            model.GetUsers = await _context.Users.ToListAsync();
            model.GetReports = await _context.Reports.ToListAsync();
            return View(model);
        }

        public async Task<IActionResult> ReportDetails(int id)
        {
            var report = await _context.Reports
                .FirstOrDefaultAsync(m => m.Id == id);
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == report.UserId);
            ViewBag.Email = user.Email;
            if (report == null)
            {
                return NotFound();
            }
            else
            {
                report.Visited = true;
                _context.Update(report);
                _context.SaveChanges();
            }
            return View(report);
        }

        public async Task<IActionResult> ExportReports()
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Reports");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "شماره گزارش";
                worksheet.Cell(currentRow, 2).Value = "موضوع گزارش";
                worksheet.Cell(currentRow, 3).Value = "متن گزارش";
                worksheet.Cell(currentRow, 4).Value = "زمان";
                worksheet.Cell(currentRow, 5).Value = "تاریخ";
                worksheet.Cell(currentRow, 6).Value = "آیدی کاربر";
                var reports = await _context.Reports.ToListAsync();
                foreach (var r in reports)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = r.Id;
                    worksheet.Cell(currentRow, 2).Value = r.Subject;
                    worksheet.Cell(currentRow, 3).Value = r.Text;
                    worksheet.Cell(currentRow, 4).Value = r.Time;
                    worksheet.Cell(currentRow, 5).Value = r.date;
                    worksheet.Cell(currentRow, 6).Value = r.UserId;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "rports.xlsx");
                }
            }
        }
    }
}