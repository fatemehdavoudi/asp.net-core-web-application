using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Kanoon.Data;
using Kanoon.Models;
using Kanoon.Models.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Kanoon.Controllers
{
    [Authorize(Roles = "User")]
    public class UserController : Controller
    {
        private string _userId;
        private string _username;
        private readonly UserManager<User> _userManager;
        private readonly ApplicationDbContext _context;

        public UserController(
            IHttpContextAccessor httpContextAccessor,
            UserManager<User> userManager,
            ApplicationDbContext context)
        {
            _userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            _username = httpContextAccessor.HttpContext.User.Identity.Name;
            _userManager = userManager;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Profile()
        {
            var username = _userManager.NormalizeName(_username);
            var user = await _userManager.FindByNameAsync(username);
            return View(user);
        }

        [HttpGet]
        public async Task<IActionResult> Edit()
        {
            var username = _userManager.NormalizeName(_username);
            var user = await _userManager.FindByNameAsync(username);
            EditViewModel model = new EditViewModel();
            model.Name = user.Name;
            model.LastName = user.LastName;
            model.NationalCode = user.NationalCode;
            model.BirthDate = user.BirthDate;
            model.Email = user.Email;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditViewModel model)
        {
            TempData["EditMessage"] = "";
            TempData["EditErrorMessage"] = "";
            var username = _userManager.NormalizeName(_username);
            var user = await _userManager.FindByNameAsync(username);
            if (ModelState.IsValid)
            {
                user.Name = model.Name;
                user.LastName = model.LastName;
                user.NationalCode = model.NationalCode;
                user.BirthDate = model.BirthDate;
                user.Email = model.Email;
                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    try
                    {
                        _context.Update(user);
                        _context.SaveChanges();
                        TempData["EditMessage"] = "ویرایش اطلاعات موفقیت انجام شد";
                        return RedirectToAction("Profile");
                    }
                    catch (Exception)
                    {
                        TempData["EditErrorMessage"] = "مجددا تلاش کنید";
                        return View(model);
                    }
                }
            }
            TempData["EditErrorMessage"] = "ویرایش اطلاعات با شکست مواجه شد";
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> AddReport()
        {
            var username = _userManager.NormalizeName(_username);
            var u = await _userManager.FindByNameAsync(username);
            PersianCalendar pc = new PersianCalendar();
            var Start = pc.ToDateTime(u.BanStart.Year, u.BanStart.Month, u.BanStart.Day, u.BanStart.Hour,
                    u.BanStart.Minute, u.BanStart.Second, u.BanStart.Millisecond);
            var End = pc.ToDateTime(u.BanEnd.Year, u.BanEnd.Month, u.BanEnd.Day, u.BanEnd.Hour,
                u.BanEnd.Minute, u.BanEnd.Second, u.BanEnd.Millisecond);
            if (DateTime.Compare(DateTime.Now, End) < 0 && DateTime.Compare(DateTime.Now, Start) >= 0)
            {
                u.IsBan = true;
                _context.Update(u);
                _context.SaveChanges();
                TempData["BanExp"] = u.BanExpretion();
                return View("BanState");
            }
            else if (DateTime.Compare(DateTime.Now, End) >= 0 || DateTime.Compare(DateTime.Now, Start) < 0)
            {
                u.IsBan = false;
                u.BanStart = new DateTime();
                u.BanEnd = new DateTime();
                _context.Update(u);
                _context.SaveChanges();
                return View();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddReport(ReportViewModel model)
        {
            TempData["ReportMessage"] = "";
            TempData["ReportErrorMessage"] = "";
            var username = _userManager.NormalizeName(_username);
            var user = await _userManager.FindByNameAsync(username);
            var time = model.EndTime.Subtract(model.StartTime);

            if (ModelState.IsValid)
            {
                var report = new Report
                {
                    Subject = model.Subject,
                    Time = time,
                    date = model.date,
                    Text = model.Text,
                    Visited = false,
                    UserId = user.Id
                };

                bool result = user.CreateReport(report);
                if (result)
                {
                    _context.Reports.Add(report);
                    await _context.SaveChangesAsync();
                    TempData["ReportMessage"] = "گزارش شما با موفقیت ثبت شد.";
                    return View(model);
                }
                TempData["ReportErrorMessage"] = "ثبت گزارش با شکست مواجه شد.";
                return View(model);
            }
            TempData["ReportErrorMessage"] = "ثبت گزارش امكان پذير نيست";
            return View(model);
        }
    }
}