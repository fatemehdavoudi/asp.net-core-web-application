using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static Kanoon.Classes.CustomValidation;

namespace Kanoon.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "وارد کردن نام الزامی است")]
        public string Name { get; set; }

        public string LastName { get; set; }


        [StringLength(10, ErrorMessage = "تعداد ارقام کد ملی قابل قبول نیست", MinimumLength = 10)]
        public string NationalCode { get; set; }

        [Required(ErrorMessage = "وارد کردن تاریخ تولد الزامی است")]
        [DataType(DataType.Date)]
        [DateLessThanOrEqualToToday]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "وارد کردن ایمیل الزامی است")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "وارد کردن رمز عبور الزامی است")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$", ErrorMessage = "رمز عبور وارد شده قابل قبول نیست")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "تایید رمز عبور الزامی است")]
        [Display(Name = "ConfirmPassword")]
        [Compare("Password", ErrorMessage = "رمز عبور و تایید رمز عبور هماهنگ نیستند.")]
        public string ConfirmPassword { get; set; }

        public bool IsAdmin { get; set; }
    }
}
