using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kanoon.Models.MultipleModels
{
    public class ReportUserViewModel
    {
        public IEnumerable<User> GetUsers { get; set; }
        public IEnumerable<Report> GetReports { get; set; }
    }
}
