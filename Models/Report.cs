using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kanoon.Models
{
    public class Report : IEquatable<Report>
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public TimeSpan Time { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime date { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public string UserId { get; set; }

        public bool Visited { get; set; }

        public bool Equals(Report r)
        {
            return this.Id == r.Id && this.UserId == r.UserId;
        }
    }
}
