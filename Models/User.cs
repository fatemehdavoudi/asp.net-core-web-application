using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kanoon.Models
{
    public class User : IdentityUser
    {
        public User()
        {
            Reports = new List<Report>();
        }

        [Required]
        public string Name { get; set; }

        [Required]
        public string LastName { get; set; }

        [StringLength(10, MinimumLength = 10, ErrorMessage = "تعداد ارقام کد ملی قابل قبول نیست")]
        public string NationalCode { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime BanEnd { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime BanStart { get; set; }

        public bool IsBan { get; set; }

        public bool IsAdmin { get; set; }
        public bool IsActive { get; set; }

        public List<Report> Reports { get; set; }

        public bool CreateReport (Report report)
        {
            Reports.Add(report);
            if (Reports.Contains(report))
            {
                return true;
            }
            return false;
        }
    }
}
