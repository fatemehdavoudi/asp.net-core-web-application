using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static Kanoon.Classes.CustomValidation;

namespace Kanoon.Models.UserViewModels
{
    public class BanViewModel
    {
        [Required(ErrorMessage = "وارد کردن تاریخ شروع الزامی است")]
        [DataType(DataType.DateTime)]
        [DateMoreThanToday]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "وارد کردن تاریخ پایان الزامی است")]
        [DataType(DataType.DateTime)]
        [DateMoreThanToday]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "وارد کردن ساعت شروع الزامی است")]
        [DataType(DataType.Time)]
        public TimeSpan StartTime { get; set; }

        [Required(ErrorMessage = "وارد کردن ساعت پایان الزامی است")]
        [DataType(DataType.Time)]
        public TimeSpan EndTime { get; set; }
    }
}
