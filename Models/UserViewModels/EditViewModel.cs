using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static Kanoon.Classes.CustomValidation;

namespace Kanoon.Models.UserViewModels
{
    public class EditViewModel
    {
        public string Name { get; set; }

        public string LastName { get; set; }

        public string NationalCode { get; set; }

        [DataType(DataType.Date)]
        [DateLessThanOrEqualToToday]
        public DateTime BirthDate { get; set; }

        [EmailAddress]
        public string Email { get; set; }
    }
}
