using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static Kanoon.Classes.CustomValidation;

namespace Kanoon.Models.UserViewModels
{
    public class ReportViewModel
    {
        [Required]
        public string Subject { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public TimeSpan StartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public TimeSpan EndTime { get; set; }
        
        [Required]
        [DataType(DataType.Date)]
        [DateLessThanOrEqualToToday]
        public DateTime date { get; set; }

        [Required]
        public string Text { get; set;}

        public bool UserBan { get; set; }
    }
}
