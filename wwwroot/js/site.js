// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function deleteAlert() {
    var Yes = document.createElement("a");
    Yes.className = "btn btn-success";
    Yes.href = "/Admin/Delete";
    swal("آیا از حذف کاربر مطمئن هستید؟", {
        dangerMode: true,
        closeOnClickOutside: false,
        closeOnEsc: false,
        content: Yes,
    }
    );
}